# Copyright 2020 Lawrence Livermore National Security, LLC and other
# minq developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: BSD-3-Clause

SLATEDIR = $(HOME)
CUDADIR = /usr/local/cuda
CXX = mpic++
CXXFLAGS = -g -Ofast -fopenmp \
	-Wall -Wextra -Wno-unused-parameter -Wno-cast-function-type \
	-I$(SLATEDIR)/include          \
	-I$(CUDADIR)/include

LDFLAGS = -L$(CUDADIR)/lib64/
LIBS = -L$(SLATEDIR)/lib/ -lslate -llapackpp -lblaspp -llapack -lblas -lcublas -lcudart -lgfortran

minq: minq.cpp
	$(CXX) $(CXXFLAGS) minq.cpp -o minq $(LDFLAGS) $(LIBS)

clean:
	rm -f minq

